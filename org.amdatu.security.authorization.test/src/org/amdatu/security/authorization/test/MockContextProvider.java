/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.authorization.AttributeQualifier;
import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.EntitySelector;
import org.amdatu.security.authorization.builder.AttributeQualifierBuilder;
import org.amdatu.security.authorization.builder.EntitySelectorBuilder;

public class MockContextProvider implements EntityProvider<MockContext> {

    private static final String CONTEXT = "Context";

    public static EntitySelector getContextSelector() {
        return EntitySelectorBuilder.build().withType(MockContext.class.getName()).withKey(CONTEXT).done();
    }

    public static AttributeQualifier getUserRoleQualifier(String userRole) {
        return AttributeQualifierBuilder.build()
            .withEntitySelector(MockContextProvider.getContextSelector())
            .withQualification(props -> props.containsKey(CONTEXT)
                && ((MockContext) props.get(CONTEXT)).get(MockContext.USER_ROLE_KEY) != null
                && ((MockContext) props.get(CONTEXT)).get(MockContext.USER_ROLE_KEY).equals(userRole))
            .done();
    }

    @Override
    public String getEntityType() {
        return MockContext.class.getName();
    }

    @Override
    public Collection<String> getRequiredPropertyKeys() {
        return Collections.emptyList();
    }

    @Override
    public Optional<MockContext> getEntity(Map<String, Object> properties) {
        return Optional.of(MockContext.get());
    }
}
