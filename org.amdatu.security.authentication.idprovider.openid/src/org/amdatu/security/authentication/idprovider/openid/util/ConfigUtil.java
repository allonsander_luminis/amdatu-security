/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.openid.util;

import java.net.URI;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.apache.felix.dm.Component;
import org.osgi.service.cm.ConfigurationException;

/**
 * Provides a couple of convenience methods to work with the OpenID-connect
 * ID-provider configuration.
 */
public final class ConfigUtil {

    public static String getMandatoryString(Dictionary<String, ?> dict, String key) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            throw new ConfigurationException(key, "not supplied!");
        }
        return val;
    }

    public static String getString(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        Object val = dict.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof String) {
            return ((String) val).trim();
        }
        throw new ConfigurationException(key, "must be a string!");
    }

    public static String getString(Map<String, String[]> params, String key) {
        String[] val = params.get(key);
        if (val == null || val.length < 1) {
            return null;
        }
        return val[0];
    }

    public static String[] getStringArray(Dictionary<String, ?> dict, String key, String[] dflt)
        throws ConfigurationException {
        Object val = dict.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof String[]) {
            return (String[]) val;
        }
        if (val instanceof String) {
            return ((String) val).trim().split("\\s*,\\s*");
        }
        throw new ConfigurationException(key, "must be a string!");
    }

    public static URI getURI(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        String val = getString(dict, key, dflt);
        if (val == null) {
            return null;
        }
        return URI.create(val);
    }

    public static void updateServiceProperties(Component comp, Object... props) {
        Dictionary<Object, Object> serviceProperties = comp.getServiceProperties();
        if (serviceProperties == null) {
            serviceProperties = new Hashtable<>();
        }
        for (int i = 0; i < props.length; i += 2) {
            serviceProperties.put(props[i].toString(), props[i + 1]);
        }
        comp.setServiceProperties(serviceProperties);
    }
}
