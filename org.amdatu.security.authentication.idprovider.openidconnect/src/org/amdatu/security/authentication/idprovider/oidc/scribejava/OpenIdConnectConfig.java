/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Defines the configuration for an OpenID connect provider. This class is compatible with
 * <a href="http://openid.net/specs/openid-connect-discovery-1_0.html">the OpenID connect discovery specification</a>.
 * <p>
 * This class provides the configuration as a simple POJO. The user of this class is responsible for making sure the
 * right values are filled in, preferably using some JSON library that directly parses the discovery result.
 * </p>
 */
public class OpenIdConnectConfig implements OpenIdConnectConstants {
    private final Map<String, Object> m_config;

    public OpenIdConnectConfig(Map<String, Object> config) {
        m_config = new HashMap<>(config);
    }

    @SuppressWarnings("unchecked")
    public static OpenIdConnectConfig parseOICConfig(InputStream is) throws IOException {
        try (Reader r = new InputStreamReader(is)) {
            Map<String, Object> config = (Map<String, Object>) new JSONParser().parse(r);

            return new OpenIdConnectConfig(config);
        }
        catch (ParseException e) {
            throw new IOException("Unable to parse OpenID connect configuration!", e);
        }
    }

    public static OpenIdConnectConfig parseOICConfig(URI configURI) throws IOException {
        URL url = configURI.toURL();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            return parseOICConfig(conn.getInputStream());
        }
        finally {
            conn.disconnect();
        }
    }

    private static boolean asBoolean(Object val) {
        if (val == null) {
            return false;
        }
        return Boolean.parseBoolean(val.toString());
    }

    private static String asString(Object val) {
        if (val == null) {
            return null;
        }
        return (String) val;
    }

    @SuppressWarnings("unchecked")
    private static List<String> asStringList(Object val) {
        if (val == null) {
            return emptyList();
        }
        if (val instanceof String) {
            return Arrays.asList((String) val);
        }
        if (val instanceof String[]) {
            return Arrays.asList((String[]) val);
        }
        return unmodifiableList((List<String>) val);
    }

    private static boolean isValidEntry(List<String> values) {
        return values != null && isValidEntry(values.toArray(new String[0]));
    }

    private static boolean isValidEntry(String... values) {
        return values != null && values.length > 0 && values[0] != null;
    }

    /**
     * @see OpenIdConnectConstants#ACR_VALUES_SUPPORTED
     */
    public List<String> getAcrValuesSupported() {
        return asStringList(m_config.get(ACR_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#AUTHORIZATION_ENDPOINT
     */
    public String getAuthorizationEndpoint() {
        return asString(m_config.get(AUTHORIZATION_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#CHECK_SESSION_IFRAME
     */
    public String getCheckSessionIframe() {
        return asString(m_config.get(CHECK_SESSION_IFRAME));
    }

    /**
     * @see OpenIdConnectConstants#CLAIMS_LOCALES_SUPPORTED
     */
    public List<String> getClaimsLocalesSupported() {
        return asStringList(m_config.get(CLAIMS_LOCALES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#CLAIMS_SUPPORTED
     */
    public List<String> getClaimsSupported() {
        return asStringList(m_config.get(CLAIMS_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#CLAIM_TYPES_SUPPORTED
     */
    public List<String> getClaimTypesSupported() {
        return asStringList(m_config.get(CLAIM_TYPES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#DISPLAY_VALUES_SUPPORTED
     */
    public List<String> getDisplayValuesSupported() {
        return asStringList(m_config.get(DISPLAY_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#END_SESSION_ENDPOINT
     */
    public String getEndSessionEndpoint() {
        return asString(m_config.get(END_SESSION_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#GRANT_TYPES_SUPPORTED
     */
    public List<String> getGrantTypesSupported() {
        return asStringList(m_config.get(GRANT_TYPES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#ID_TOKEN_ENCRYPTION_ALG_VALUES_SUPPORTED
     */
    public List<String> getIdTokenEncryptionAlgValuesSupported() {
        return asStringList(m_config.get(ID_TOKEN_ENCRYPTION_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#ID_TOKEN_ENCRYPTION_ENC_VALUES_SUPPORTED
     */
    public List<String> getIdTokenEncryptionEncValuesSupported() {
        return asStringList(m_config.get(ID_TOKEN_ENCRYPTION_ENC_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#ID_TOKEN_SIGNING_ALG_VALUES_SUPPORTED
     */
    public List<String> getIdTokenSigningAlgValuesSupported() {
        return asStringList(m_config.get(ID_TOKEN_SIGNING_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#ISSUER
     */
    public String getIssuer() {
        return asString(m_config.get(ISSUER));
    }

    /**
     * @see OpenIdConnectConstants#JWKS_URI
     */
    public String getJwksUri() {
        return asString(m_config.get(JWKS_URI));
    }

    /**
     * @see OpenIdConnectConstants#OP_POLICY_URI
     */
    public String getOpPolicyUri() {
        return asString(m_config.get(OP_POLICY_URI));
    }

    /**
     * @see OpenIdConnectConstants#OP_TOS_URI
     */
    public String getOpTosUri() {
        return asString(m_config.get(OP_TOS_URI));
    }

    /**
     * @see OpenIdConnectConstants#REGISTRATION_ENDPOINT
     */
    public String getRegistrationEndpoint() {
        return asString(m_config.get(REGISTRATION_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#REQUEST_OBJECT_ENCRYPTION_ALG_VALUES_SUPPORTED
     */
    public List<String> getRequestObjectEncryptionAlgValuesSupported() {
        return asStringList(m_config.get(REQUEST_OBJECT_ENCRYPTION_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#REQUEST_OBJECT_ENCRYPTION_ENC_VALUES_SUPPORTED
     */
    public List<String> getRequestObjectEncryptionEncValuesSupported() {
        return asStringList(m_config.get(REQUEST_OBJECT_ENCRYPTION_ENC_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#REQUEST_OBJECT_SIGNING_ALG_VALUES_SUPPORTED
     */
    public List<String> getRequestObjectSigningAlgValuesSupported() {
        return asStringList(m_config.get(REQUEST_OBJECT_SIGNING_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#RESPONSE_MODES_SUPPORTED
     */
    public List<String> getResponseModesSupported() {
        return asStringList(m_config.get(RESPONSE_MODES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#RESPONSE_TYPES_SUPPORTED
     */
    public List<String> getResponseTypesSupported() {
        return asStringList(m_config.get(RESPONSE_TYPES_SUPPORTED));
    }

    /**
     * @return OpenIdConnectConstants#REVOCATION_ENDPOINT
     */
    public String getRevocationEndpoint() {
        return asString(m_config.get(REVOCATION_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#SCOPES_SUPPORTED
     */
    public List<String> getScopesSupported() {
        return asStringList(m_config.get(SCOPES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#SERVICE_DOCUMENTATION
     */
    public String getServiceDocumentation() {
        return asString(m_config.get(SERVICE_DOCUMENTATION));
    }

    /**
     * @see OpenIdConnectConstants#SUBJECT_TYPES_SUPPORTED
     */
    public List<String> getSubjectTypesSupported() {
        return asStringList(m_config.get(SUBJECT_TYPES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#TOKEN_ENDPOINT
     */
    public String getTokenEndpoint() {
        return asString(m_config.get(TOKEN_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#TOKEN_ENDPOINT_AUTH_METHODS_SUPPORTED
     */
    public List<String> getTokenEndpointAuthMethodsSupported() {
        return asStringList(m_config.get(TOKEN_ENDPOINT_AUTH_METHODS_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#TOKEN_ENDPOINT_AUTH_SIGNING_ALG_VALUES_SUPPORTED
     */
    public List<String> getTokenEndpointAuthSigningAlgValuesSupported() {
        return asStringList(m_config.get(TOKEN_ENDPOINT_AUTH_SIGNING_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#UI_LOCALES_SUPPORTED
     */
    public List<String> getUiLocalesSupported() {
        return asStringList(m_config.get(UI_LOCALES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#USERINFO_ENCRYPTION_ALG_VALUES_SUPPORTED
     */
    public List<String> getUserinfoEncryptionAlgValuesSupported() {
        return asStringList(m_config.get(USERINFO_ENCRYPTION_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#USERINFO_ENCRYPTION_ENC_VALUES_SUPPORTED
     */
    public List<String> getUserinfoEncryptionEncValuesSupported() {
        return asStringList(m_config.get(USERINFO_ENCRYPTION_ENC_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#USERINFO_ENDPOINT
     */
    public String getUserinfoEndpoint() {
        return asString(m_config.get(USERINFO_ENDPOINT));
    }

    /**
     * @see OpenIdConnectConstants#USERINFO_SIGNING_ALG_VALUES_SUPPORTED
     */
    public List<String> getUserinfoSigningAlgValuesSupported() {
        return asStringList(m_config.get(USERINFO_SIGNING_ALG_VALUES_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#CLAIMS_PARAMETER_SUPPORTED
     */
    public boolean isClaimsParameterSupported() {
        return asBoolean(m_config.get(CLAIMS_PARAMETER_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#REQUEST_PARAMETER_SUPPORTED
     */
    public boolean isRequestParameterSupported() {
        return asBoolean(m_config.get(REQUEST_PARAMETER_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#REQUEST_URI_PARAMETER_SUPPORTED
     */
    public boolean isRequestUriParameterSupported() {
        return asBoolean(m_config.get(REQUEST_URI_PARAMETER_SUPPORTED));
    }

    /**
     * @see OpenIdConnectConstants#REQUIRE_REQUEST_URI_REGISTRATION
     */
    public boolean isRequireRequestUriRegistration() {
        return asBoolean(m_config.get(REQUIRE_REQUEST_URI_REGISTRATION));
    }

    /**
     * @return <code>true</code> if this configuration is valid, which means that all required properties of the core
     *         discovery specification are properly filled.
     */
    public boolean isValid() {
        return isValidEntry(getIssuer()) &&
            isValidEntry(getAuthorizationEndpoint()) &&
            isValidEntry(getJwksUri()) &&
            isValidEntry(getResponseTypesSupported()) &&
            isValidEntry(getSubjectTypesSupported()) &&
            isValidEntry(getIdTokenSigningAlgValuesSupported());
    }
}
