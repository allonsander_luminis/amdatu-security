/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import java.util.regex.Pattern;

import com.github.scribejava.core.extractors.OAuth2AccessTokenJsonExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;

/**
 * additionally parses OpenID id_token
 */
public class OpenIdConnectJsonTokenExtractor extends OAuth2AccessTokenJsonExtractor {
    private static final Pattern ID_TOKEN_REGEX = Pattern.compile("\"id_token\"\\s*:\\s*\"(\\S*?)\"");

    protected OpenIdConnectJsonTokenExtractor() {
        // Nop
    }

    private static class InstanceHolder {
        private static final OpenIdConnectJsonTokenExtractor INSTANCE = new OpenIdConnectJsonTokenExtractor();
    }

    public static OpenIdConnectJsonTokenExtractor instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected OAuth2AccessToken createToken(String accessToken, String tokenType, Integer expiresIn,
        String refreshToken, String scope, String response) {
        return new OpenIdToken(accessToken, tokenType, expiresIn, refreshToken, scope,
            extractParameter(response, ID_TOKEN_REGEX, false), response);
    }
}
