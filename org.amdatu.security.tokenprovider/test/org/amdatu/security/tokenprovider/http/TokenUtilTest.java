/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.http;

import static org.amdatu.security.tokenprovider.http.TokenUtil.AUTHORIZATION_HEADER;
import static org.amdatu.security.tokenprovider.http.TokenUtil.getTokenFromHeader;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class TokenUtilTest {
    @Mock
    private HttpServletRequest request;

    @Test
    public void testGetTokenFromHeader() {
        String authSchemeName = "authScheme";

        String token = "MyNotSoSecretToken";
        String authHeader = String.format("%s %s", authSchemeName, token);

        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn(authHeader);

        String tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, equalTo(token));

        String authHeaderWithoutSpace = String.format("%s%s", authSchemeName, token);
        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn(authHeaderWithoutSpace);

        tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, nullValue());

        when(request.getHeader(eq(AUTHORIZATION_HEADER))).thenReturn("Basic base64thing");

        tokenFromRequest = getTokenFromHeader(request, authSchemeName);
        assertThat(tokenFromRequest, nullValue());
    }
}
