/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider;

import java.util.Map;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Provides the ability to intervene with the token verification process.
 * <p>
 * The {@link #verifyToken(Map)} is called for all tokens that are syntactically correct,
 * that is, an opaque token was correctly transformed into a set of claims. Token
 * verification interceptors are not called for tokens that are not syntactically
 * correct.
 * <p>
 * Token verification interceptors can be used, for example, to check whether or not
 * tokens are revoked or any other "advanced" check that should be done for each
 * token. Note that these interceptors are called for every verification request, so
 * performance might be impacted by heavy operations done inside custom interceptors!
 * <p>
 * The first token verification interceptor that cannot verify a given set of claims as
 * correct has to throw a (subtype of) {@link TokenProviderException}.
 */
@ConsumerType
public interface TokenVerificationInterceptor {

    /**
     * Verifies whether the given token claims are correct or not.
     *
     * @param claims the set of (flattened) token claims, never <code>null</code>.
     * @throws IllegalArgumentException in case the given claims were <code>null</code>;
     * @throws TokenProviderException in case of verification problems of the given claims.
     */
    void verifyToken(Map<String, String> claims) throws IllegalArgumentException, TokenProviderException;

}
