/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import static org.amdatu.security.tokenprovider.TokenConstants.SUBJECT;
import static org.amdatu.security.tokenprovider.jwt.JwtConstants.KEY_ISSUER;
import static org.jose4j.jwt.ReservedClaimNames.EXPIRATION_TIME;
import static org.jose4j.jwt.ReservedClaimNames.ISSUED_AT;
import static org.jose4j.jwt.ReservedClaimNames.NOT_BEFORE;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.security.Key;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenProviderException;
import org.amdatu.security.tokenprovider.TokenVerificationInterceptor;
import org.amdatu.security.tokenprovider.jwt.JwtKey;
import org.amdatu.security.tokenprovider.jwt.JwtKeyProvider;
import org.apache.felix.dm.Component;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.InvalidJwtSignatureException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.keys.resolvers.VerificationKeyResolver;
import org.jose4j.lang.JoseException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

/**
 * Provides a token provider that uses JSON Web Tokens.
 */
public class TokenProviderImpl implements TokenProvider, JwtKeyProvider, ManagedService {
    private final Map<ServiceReference<TokenVerificationInterceptor>, TokenVerificationInterceptor> m_interceptors;
    // Injected by Felix DM...
    private volatile Component m_component;
    private volatile LogService m_log;
    // Locally managed
    private VerificationKeyResolver m_keyResolver;
    private volatile TokenProviderConfig m_config;

    /**
     * Creates a new {@link TokenProviderImpl} instance.
     */
    public TokenProviderImpl() {
        m_interceptors = new HashMap<>();
        m_config = new TokenProviderConfig();
        m_keyResolver = new JwtVerificationKeyResolver();
    }

    @Override
    public String generateToken(Map<String, String> attributes) throws TokenProviderException {
        if (attributes == null) {
            throw new IllegalArgumentException("Attributes cannot be null!");
        }
        return generateToken(Instant.now(), attributes);
    }

    @Override
    public List<JwtKey> getVerificationKeys() {
        return Arrays.asList(new JwtKeyImpl(getConfig().getVerificationKey()));
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        TokenProviderConfig newConfig;
        if (properties == null) {
            newConfig = new TokenProviderConfig();
        }
        else {
            newConfig = new TokenProviderConfig(properties);
        }
        m_config = newConfig;

        updateServiceProperties();
    }

    @Override
    public Map<String, String> verifyToken(String token) throws TokenProviderException, InvalidTokenException {
        if (token == null) {
            throw new IllegalArgumentException("Token cannot be null!");
        }

        JwtContext context = null;
        try {
            // Do a two-staged parsing of the token, first we need to get the issuer to get the right JwtTokenVerifier...
            JwtConsumer consumer = new JwtConsumerBuilder()
                .setSkipAllValidators()
                .setSkipSignatureVerification()
                .build();

            context = consumer.process(token);

            String issuer = context.getJwtClaims().getIssuer();

            JwtConsumerBuilder builder = new JwtConsumerBuilder()
                .setExpectedIssuer(issuer)
                .setRequireExpirationTime()
                .setRequireSubject()
                .setSkipDefaultAudienceValidation()
                .setVerificationKeyResolver(m_keyResolver);

            TokenProviderConfig config = getConfig();
            if (config.getAllowedClockSkew() > 0) {
                builder.setAllowedClockSkewInSeconds(config.getAllowedClockSkew());
            }

            // Re-process the context with the new rules...
            builder.build().processContext(context);

            Map<String, String> claims = getAttributesFrom(context.getJwtClaims());

            // Call each of the verification interceptors...
            verifyTokenClaims(claims);

            return claims;
        }
        catch (InvalidJwtSignatureException e) {
            Map<String, String> attrs = context != null ? getAttributesFrom(context.getJwtClaims()) : null;
            log(LOG_WARNING, "Failed to verify token! %s", e.getMessage());
            throw new InvalidTokenException(Reason.INVALID_SIGNATURE, "Token has an invalid signature!", attrs);
        }
        catch (InvalidJwtException e) {
            String msg = e.getMessage();
            Map<String, String> attrs = context != null ? getAttributesFrom(context.getJwtClaims()) : null;

            log(LOG_WARNING, "Failed to verify token! %s", msg);

            if (msg.contains("The JWT is not yet valid ")) {
                throw new InvalidTokenException(Reason.TOKEN_NOT_VALID, "Token is not valid yet!", e, attrs);
            }
            else if (msg.contains("The JWT is no longer valid ")) {
                throw new InvalidTokenException(Reason.TOKEN_EXPIRED, "Token is expired!", e, attrs);
            }

            throw new InvalidTokenException(Reason.UNKNOWN, "Token is invalid!", e, attrs);
        }
        catch (MalformedClaimException e) {
            Map<String, String> attrs = context != null ? getAttributesFrom(context.getJwtClaims()) : null;
            log(LOG_WARNING, "Failed to verify token! %s", e.getMessage());
            throw new InvalidTokenException(Reason.MALFORMED_TOKEN, "Token has an invalid claim!", e, attrs);
        }
    }

    // Called by Felix DM...
    protected final void addInterceptor(ServiceReference<TokenVerificationInterceptor> srvReg, TokenVerificationInterceptor srv) {
        m_interceptors.put(srvReg, srv);
    }

    // Called by Felix DM...
    protected final void removeInterceptor(ServiceReference<TokenVerificationInterceptor> srvReg, TokenVerificationInterceptor srv) {
        m_interceptors.remove(srvReg);
    }

    protected String generateToken(Instant now, Map<String, String> attributes) {
        TokenProviderConfig config = getConfig();

        JwtClaims claims = createClaims(config, now, new HashMap<>(attributes));

        return createSignedToken(claims, config.getAlgorithm(), config.getSigningKey());
    }

    protected final TokenProviderConfig getConfig() {
        return m_config;
    }

    protected final void setVerificationKeyResolver(VerificationKeyResolver keyResolver) {
        m_keyResolver = keyResolver;
    }

    /**
     * Called by Felix DM.
     */
    protected final void start(Component c) {
        updateServiceProperties();
    }

    private JwtClaims createClaims(TokenProviderConfig config, Instant issuedAt, Map<String, String> attributes) {
        long iss = getTimeValue(ISSUED_AT, attributes, issuedAt.getEpochSecond());
        long nbf = getTimeValue(NOT_BEFORE, attributes, iss);
        long exp = getTimeValue(EXPIRATION_TIME, attributes, nbf + config.getTokenValidityPeriod());

        JwtClaims claims = new JwtClaims();
        claims.setIssuer(config.getIssuer());
        claims.setAudience(config.getAudience());
        claims.setSubject(attributes.get(SUBJECT));
        claims.setGeneratedJwtId();
        claims.setIssuedAt(NumericDate.fromSeconds(iss));
        claims.setNotBefore(NumericDate.fromSeconds(nbf));
        claims.setExpirationTime(NumericDate.fromSeconds(exp));
        for (Entry<String, String> entry : attributes.entrySet()) {
            claims.setClaim(entry.getKey(), entry.getValue());
        }
        return claims;
    }

    private String createSignedToken(JwtClaims claims, String algorithm, Key signingKey) throws TokenProviderException {
        JsonWebSignature jws = new JsonWebSignature();
        jws.setAlgorithmConstraints(AlgorithmConstraints.DISALLOW_NONE);
        jws.setAlgorithmHeaderValue(algorithm);
        jws.setPayload(claims.toJson());
        jws.setDoKeyValidation(true);
        jws.setKey(signingKey);

        try {
            return jws.getCompactSerialization();
        }
        catch (JoseException e) {
            log(LOG_WARNING, "Unable to generate and serialize JWT token!", e);

            throw new TokenProviderException("Unable to generate token!", e);
        }
    }

    private Map<String, String> getAttributesFrom(JwtClaims jwtClaims) {
        Map<String, List<Object>> flatClaims = jwtClaims.flattenClaims();

        Map<String, String> result = new TreeMap<>();
        for (Entry<String, List<Object>> entry : flatClaims.entrySet()) {
            String name = entry.getKey();
            List<Object> claims = entry.getValue();

            int size = claims.size();
            Object value = (size > 0) ? claims.get(0) : null;

            if (size > 1) {
                log(LOG_WARNING, "Multiple (%d) values found for claim '%s', only returning the first one!",
                    claims.size(), name);
            }

            result.put(name, Objects.toString(value, null));
        }
        return result;
    }

    private long getTimeValue(String key, Map<String, String> attrs, long dflt) {
        String v = attrs.remove(key);
        if (v != null && !"".equals(v.trim())) {
            try {
                return Long.valueOf(v);
            }
            catch (NumberFormatException e) {
                // Ignore, return the default below...
            }
        }
        return dflt;
    }

    private void log(int level, String msg, Object... args) {
        log(level, msg, null, args);
    }

    private void log(int level, String msg, Throwable t, Object... args) {
        if (m_log != null) {
            if (t != null) {
                m_log.log(level, String.format(msg, args), t);
            }
            else {
                m_log.log(level, String.format(msg, args));
            }
        }
    }

    private void updateServiceProperties() {
        if (m_component != null) {
            Dictionary<String, Object> srvProps = m_component.getServiceProperties();
            srvProps.put(KEY_ISSUER, m_config.getIssuer());

            // propagate properties to component...
            m_component.setServiceProperties(srvProps);
        }
    }

    private void verifyTokenClaims(Map<String, String> claims) {
        List<TokenVerificationInterceptor> interceptors = new ArrayList<>(m_interceptors.values());
        for (TokenVerificationInterceptor interceptor : interceptors) {
            interceptor.verifyToken(claims);
        }
    }
}
