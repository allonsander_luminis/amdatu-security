/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import static org.amdatu.security.tokenprovider.jwt.JwtConstants.KEY_ISSUER;
import static org.amdatu.security.tokenprovider.jwt.JwtConstants.KEY_URL;

import java.security.Key;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.jwt.JwtKey;
import org.amdatu.security.tokenprovider.jwt.JwtKeyProvider;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.VerificationJwkSelector;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwx.JsonWebStructure;
import org.jose4j.keys.resolvers.VerificationKeyResolver;
import org.jose4j.lang.JoseException;
import org.jose4j.lang.UnresolvableKeyException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Custom {@link VerificationKeyResolver} implementation that allows us to plug custom key providers.
 */
public class JwtVerificationKeyResolver implements VerificationKeyResolver {
    /** RegEx that matches the issuer definition as present in the raw (unverified) token payload. */
    private static final Pattern ISS_RE = Pattern.compile("\"iss\"\\s*:\\s*\"([^\"]+)\"");

    @Override
    public Key resolveKey(JsonWebSignature sig, List<JsonWebStructure> struct) throws UnresolvableKeyException {
        String issuer = getIssuer(sig);
        try {
            List<JsonWebKey> keys = findJsonWebKeys(issuer);

            JsonWebKey selected = new VerificationJwkSelector().select(sig, keys);
            if (selected == null) {
                throw new UnresolvableKeyException("Unable to resolve key for issuer: " + issuer);
            }

            return selected.getKey();
        }
        catch (InvalidSyntaxException | JoseException e) {
            throw new UnresolvableKeyException("Failed to resolve key for issuer: " + issuer, e);
        }
    }

    private String getIssuer(JsonWebSignature sig) throws UnresolvableKeyException {
        Matcher m = ISS_RE.matcher(sig.getUnverifiedPayload());
        if (m.find()) {
            return m.group(1);
        }
        throw new UnresolvableKeyException("Unable to determine issuer for signature!");
    }

    private List<JsonWebKey> findJsonWebKeys(String issuer) throws InvalidSyntaxException {
        return findKeys(issuer)
            .map(JwtVerificationKeyResolver::toJsonWebKey)
            .collect(Collectors.toList());
    }

    private Stream<JwtKey> findKeys(String issuer) throws InvalidSyntaxException {
        return findJwtKeyProviders(issuer)
            .map(JwtKeyProvider::getVerificationKeys)
            .flatMap(Collection::stream);
    }

    private Stream<JwtKeyProvider> findJwtKeyProviders(String issuer) throws InvalidSyntaxException {
        String filter = String.format("(&(%s=*)(!(%s=*)))", KEY_ISSUER, KEY_URL);

        BundleContext ctx = FrameworkUtil.getBundle(getClass()).getBundleContext();
        return ctx.getServiceReferences(JwtKeyProvider.class, filter).stream()
            .filter(sr -> matchIssuer(sr, issuer))
            .map(ctx::getService);
    }

    private static boolean matchIssuer(ServiceReference<?> serviceRef, String wantedIssuer) {
        String issuer = (String) serviceRef.getProperty(KEY_ISSUER);
        return wantedIssuer.equals(issuer) || wantedIssuer.matches(issuer);
    }

    private static JsonWebKey toJsonWebKey(JwtKey jwtKey) {
        try {
            JsonWebKey webKey = JsonWebKey.Factory.newJwk(jwtKey.getKey());
            jwtKey.getKeyID().ifPresent(webKey::setKeyId);
            return webKey;
        }
        catch (JoseException e) {
            throw new InvalidTokenException(Reason.MALFORMED_TOKEN, "Unable to convert JwtKey to JsonWebKey!", e, null);
        }
    }
}
