/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt;

import java.security.Key;
import java.util.Optional;

/**
 * Represents a JSON web token key.
 */
public interface JwtKey {

    /**
     * @return the actual key implementation, cannot be <code>null</code>.
     */
    Key getKey();

    /**
     * @return the optional key ID of this key, as provided in the JSON structure.
     */
    Optional<String> getKeyID();
}
