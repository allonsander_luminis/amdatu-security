/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.hash.pbkdf2;

import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Dictionary;

import org.amdatu.security.password.hash.PasswordHasher;
import org.amdatu.security.password.hash.PasswordHasherException;
import org.amdatu.security.util.crypto.Subtle;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * Default implementation of {@link PasswordHasher} that, by default, provides a PBKDF-based hashing implementation.
 */
public class Pbkdf2Hasher implements PasswordHasher, ManagedService {
    private static final String NOT_SET_HASH = "$notset$";

    private volatile Pbkdf2Config m_config;

    /**
     * Creates a new {@link Pbkdf2Hasher} instance.
     */
    public Pbkdf2Hasher() {
        m_config = new Pbkdf2Config();
    }

    @Override
    public String hash(String input) {
        Pbkdf2Config cfg = m_config;

        try {
            int iterations = cfg.getIterations();
            byte[] salt = cfg.generateSalt();
            byte[] hashed = cfg.generateHash(input, salt);

            return encode(hashed, salt, iterations);
        }
        catch (InvalidKeySpecException e) {
            throw new PasswordHasherException("Unable to hash password!", e);
        }
    }

    @Override
    public boolean isHashed(String input) {
        if (input == null || input.length() < 6) {
            return false;
        }
        byte[] b = input.getBytes();
        return b[0] == '$' && b[1] == 's' && b[2] == 'h' && b[3] == 'a' && b[4] == '1' && b[5] == '$';
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        Pbkdf2Config newConfig;
        if (properties == null) {
            newConfig = new Pbkdf2Config();
        }
        else {
            newConfig = new Pbkdf2Config(properties);
        }
        m_config = newConfig;
    }

    @Override
    public boolean verify(String hash, String input) {
        Pbkdf2Config cfg = m_config;

        if (NOT_SET_HASH.equals(hash) || NOT_SET_HASH.equals(input)) {
            return false;
        }
        else if (!isHashed(hash)) {
            return Subtle.isEqual(hash.getBytes(), input.getBytes());
        }

        String[] parts = hash.split("\\$");
        if (parts.length < 4 || !"".equals(parts[0]) || !"sha1".equals(parts[1])) {
            throw new PasswordHasherException("Invalid password hash: invalid format!");
        }
        int iterations;
        try {
            iterations = Integer.valueOf(parts[2]);
        }
        catch (NumberFormatException e) {
            throw new PasswordHasherException("Invalid password hash: invalid format!", e);
        }

        try {
            Decoder decoder = Base64.getDecoder();
            byte[] salt = decoder.decode(parts[3]);
            byte[] expectedPwdHash = decoder.decode(parts[4]);

            byte[] pwdHash = cfg.generateHash(input, salt, iterations, expectedPwdHash.length);

            return Subtle.isEqual(expectedPwdHash, pwdHash);
        }
        catch (InvalidKeySpecException e) {
            throw new PasswordHasherException("Unable to verify password!", e);
        }
    }

    private String encode(byte[] hashed, byte[] salt, int iterations) {
        Encoder encoder = Base64.getEncoder().withoutPadding();
        StringBuilder sb = new StringBuilder();
        sb.append("$sha1$");
        sb.append(iterations);
        sb.append('$');
        sb.append(encoder.encodeToString(salt));
        sb.append('$');
        sb.append(encoder.encodeToString(hashed));
        return sb.toString();
    }
}
