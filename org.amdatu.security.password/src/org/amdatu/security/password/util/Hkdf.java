/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.util;

import static java.util.Objects.requireNonNull;

import java.security.GeneralSecurityException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Provides a HMAC-based key derivation function, as detailed in <a href="https://tools.ietf.org/html/rfc5869">RFC 5869</a>.
 */
public class Hkdf {

    public static class Builder {
        private String m_macAlg;
        private byte[] m_initialKeyingMaterial;
        private byte[] m_salt;

        public Builder() {
            m_macAlg = DEFAULT_HMAC;
        }

        public Hkdf build() {
            if (m_initialKeyingMaterial == null || m_initialKeyingMaterial.length < 1) {
                throw new IllegalArgumentException("Invalid initial keying material: cannot be null or empty!");
            }

            try {
                return new Hkdf(m_macAlg, m_initialKeyingMaterial, m_salt);
            }
            finally {
                m_initialKeyingMaterial = null;
                m_salt = null;
            }
        }

        public Builder setAlgorithm(String macAlg) {
            m_macAlg = requireNonNull(macAlg);
            return this;
        }

        public Builder setInitialKeyingMaterial(byte[] ikm) {
            m_initialKeyingMaterial = requireNonNull(ikm);
            return this;
        }

        public Builder setSalt(byte[] salt) {
            m_salt = salt;
            return this;
        }
    }

    private static final String DEFAULT_HMAC = "HmacSHA256";

    private final Mac m_mac;

    /**
     * Creates a new {@link Hkdf} instance.
     */
    private Hkdf(String alg, byte[] ikm, byte[] salt) {
        try {
            m_mac = Mac.getInstance(alg);

            int macLength = m_mac.getMacLength();

            if (salt == null || salt.length == 0) {
                salt = new byte[macLength];
                Arrays.fill(salt, (byte) 0);
            }

            m_mac.init(new SecretKeySpec(salt, alg));
            byte[] rawKeyMaterial = m_mac.doFinal(ikm);

            m_mac.init(new SecretKeySpec(rawKeyMaterial, alg));

            Arrays.fill(rawKeyMaterial, (byte) 0);
        }
        catch (GeneralSecurityException e) {
            throw new RuntimeException("Unable to create MAC instance!", e);
        }
    }

    /**
     * Derives a secret key from the original keying material in such way that there it is
     * computationally infeasible to reconstruct the original keying material from the output of
     * this method.
     *
     * @param info the optional information for the secret key. If this instance is reused for
     *        multiple keys, one should provide unique values on each call. Can be <code>null</code>
     *        if this method is called only once for some keying material;
     * @param length the length, in bytes, of the resulting secret key.
     * @return the derived secret key, never <code>null</code>.
     */
    public byte[] deriveKey(byte[] info, int length) {
        if (length < 1) {
            throw new IllegalArgumentException("Invalid length: cannot be less than 1!");
        }
        if (length > (255 * m_mac.getMacLength())) {
            throw new IllegalArgumentException("Invalid length: length can not be longer than 255 times the HMAC length!");
        }

        byte[] output = new byte[length];

        if (info == null) {
            info = new byte[0];
        }

        m_mac.reset();

        byte[] tmp = new byte[0];
        try {
            int loc = 0;
            byte b = 1;

            while (loc < length) {
                m_mac.update(tmp);
                m_mac.update(info);
                m_mac.update(b++);
                tmp = m_mac.doFinal();

                for (int x = 0; x < tmp.length && loc < length; x++, loc++) {
                    output[loc] = tmp[x];
                }
            }

            return output;
        }
        finally {
            Arrays.fill(tmp, (byte) 0);
        }
    }
}
