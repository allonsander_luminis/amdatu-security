/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rbac;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.amdatu.security.authorization.OptionalBiPredicate;

public class HasRolePredicate implements OptionalBiPredicate<Map<String, Object>, Map<String, Object>> {

    private final Set<String> m_roles;

    public static HasRolePredicate withRoles(String... roles) {
        return new HasRolePredicate(roles);
    }

    private HasRolePredicate(String... roles) {
        m_roles = new HashSet<>(Arrays.asList(roles));
    }

    @Override
    public Optional<Boolean> test(Map<String, Object> subjectAttrs, Map<String, Object> resourceAttrs) {
        @SuppressWarnings("unchecked")
        Set<String> subjectRoles = (Set<String>)subjectAttrs.get(SubjectRoleEntityProvider.ENTITY_TYPE);

        if (Collections.disjoint(m_roles, subjectRoles)) {
            return Optional.empty();
        } else {
            return Optional.of(true);
        }
    }

}