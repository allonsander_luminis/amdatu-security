/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.AccountAdminBackend;

/**
 * In memory backend for {@link AccountAdminImpl}, used in unit tests.
 */
class MockAccountAdminBackend implements AccountAdminBackend {
    private final Map<String, Account> m_accounts;
    private boolean m_removalSupported = true;

    public MockAccountAdminBackend() {
        m_accounts = new HashMap<>();
    }

    @Override
    public boolean accountExists(String accountId) {
        return m_accounts.containsKey(accountId);
    }

    @Override
    public void create(Account account) {
        m_accounts.putIfAbsent(account.getId(), account);
    }

    @Override
    public Optional<Account> get(String accountId) {
        return Optional.ofNullable(m_accounts.get(accountId));
    }

    @Override
    public void remove(Account account) {
        if (!m_removalSupported) {
            throw new UnsupportedOperationException();
        }
        m_accounts.remove(account.getId());
    }

    @Override
    public void update(Account account) {
        m_accounts.put(account.getId(), account);
    }

    public void setRemovalSupported(boolean removalSupported) {
        m_removalSupported = removalSupported;
    }
}
