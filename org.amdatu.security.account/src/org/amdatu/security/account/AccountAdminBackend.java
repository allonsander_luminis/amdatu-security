/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

import java.util.Optional;

/**
 * Backend service for storing and accessing {@link Account}s from long-term storage.
 */
public interface AccountAdminBackend {

    /**
     * Returns whether or not an account with the given identifier exists.
     *
     * @param accountId the identifier of the account to test for, cannot be <code>null</code>.
     * @return <code>true</code> if an account exists with the given identifier, <code>false</code> otherwise.
     */
    boolean accountExists(String accountId);

    /**
     * Creates a new account with the given information.
     *
     * @param account the new account to create, cannot be <code>null</code>.
     * @throws AccountExistsException in case an attempt is made to create an account with an existing identifier.
     */
    void create(Account account) throws AccountExistsException;

    /**
     * Retrieves an account with the given identifier.
     *
     * @param accountId the identifier of the account to retrieve, cannot be <code>null</code>.
     * @return an optional account, that can be empty when no account with the given identifier exists.
     */
    Optional<Account> get(String accountId);

    /**
     * Removes an account.
     * <p>
     * This is an optional operation. By default, this method will throw an {@link UnsupportedOperationException}.
     * </p>
     *
     * @param account the account to update, cannot be <code>null</code>.
     * @throws NoSuchAccountException in case the given account was not present in the backend.
     * @throws UnsupportedOperationException in case this operation is not supported by the backend.
     */
    default void remove(Account account) {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates the account.
     *
     * @param account the account to update, cannot be <code>null</code>.
     * @throws NoSuchAccountException in case the given account was not present in the backend.
     */
    void update(Account account);

}
