/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.security.account.admin;

import static java.util.stream.Collectors.toMap;

import java.util.Dictionary;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.AccountAdmin;
import org.amdatu.security.account.AccountLockedException;
import org.amdatu.security.account.NoSuchAccountException;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * Adapter for {@link AccountAdmin}s to provide a {@link LocalIdCredentialsProvider}.
 */
public class AccountCredentialsProvider implements LocalIdCredentialsProvider, ManagedService {
    // Managed by Felix DM...
    private volatile AccountAdmin m_accountAdmin;
    // locally managed
    private volatile AccountAdminConfig m_config;

    public AccountCredentialsProvider() {
        m_config = new AccountAdminConfig();
    }

    @Override
    public Optional<String> getId(Map<String, String> credentials) {
        return m_accountAdmin.getAccount(credentials)
            .map(Account::getId);
    }

    @Override
    public Optional<Map<String, String>> getPublicCredentials(String localId) {
        try {
            Map<String, String> credentials = m_accountAdmin.getAccountById(localId)
                .getCredentials().entrySet().stream()
                .filter(e -> isPublicCredential(e.getKey()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

            return Optional.of(credentials);
        }
        catch (AccountLockedException | NoSuchAccountException e) {
            return Optional.empty();
        }
    }

    private boolean isPublicCredential(String key) {
        return !m_config.getSensitiveCredentials().contains(key);
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        AccountAdminConfig newConfig;
        if (properties != null) {
            newConfig = new AccountAdminConfig(properties);
        }
        else {
            newConfig = new AccountAdminConfig();
        }
        m_config = newConfig;
    }
}
