/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static java.lang.String.format;
import static org.amdatu.security.account.admin.rest.AccountAdminContext.DEFAULT_CTX_PATH;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_NAME;
import static org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants.JAX_RS_RESOURCE;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.ws.rs.core.Application;

import org.amdatu.security.account.AccountAdmin;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    private static final String RES_PID = "org.amdatu.security.account.admin.resource";
    private static final String CTX_PID = "org.amdatu.security.account.admin.context";

    private static final String CTX_AMDATU_ACCOUNT_ADM = "org.amdatu.security.account.admin";
    private static final String JAX_RS_APP_AMDATU_ACCOUNT_ADM = "org.amdatu.security.account.admin";

    @Override
    public void init(BundleContext context, DependencyManager dm) {
        Dictionary<String, Object> props = new Hashtable<>();

        props.put(JAX_RS_APPLICATION_BASE, "/rest");
        props.put(JAX_RS_APPLICATION_CONTEXT, CTX_AMDATU_ACCOUNT_ADM);
        props.put(JAX_RS_NAME, JAX_RS_APP_AMDATU_ACCOUNT_ADM);

        dm.add(createComponent()
            .setInterface(Application.class.getName(), props)
            .setImplementation(AccountAdminApplication.class));

        props.put(Constants.SERVICE_PID, RES_PID);
        props.put(JAX_RS_RESOURCE, "true");
        props.put(JAX_RS_APPLICATION_SELECT, format("(%s=%s)", JAX_RS_NAME, JAX_RS_APP_AMDATU_ACCOUNT_ADM));
        dm.add(createComponent()
            .setInterface(new String[] { ManagedService.class.getName() }, props)
            .setImplementation(AccountAdminResource.class)
            .add(createServiceDependency().setService(AccountAdmin.class).setRequired(true))
            .add(createServiceDependency().setService(EventAdmin.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, CTX_PID);
        props.put(HTTP_WHITEBOARD_CONTEXT_PATH, DEFAULT_CTX_PATH);
        props.put(HTTP_WHITEBOARD_CONTEXT_NAME, CTX_AMDATU_ACCOUNT_ADM);

        dm.add(createComponent()
            .setInterface(new String[] { ManagedService.class.getName(), ServletContextHelper.class.getName() }, props)
            .setImplementation(AccountAdminContext.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        props = new Hashtable<>();
        props.put(HTTP_WHITEBOARD_RESOURCE_PREFIX, "/res");
        props.put(HTTP_WHITEBOARD_RESOURCE_PATTERN, "/*");
        props.put(HTTP_WHITEBOARD_CONTEXT_SELECT, format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, CTX_AMDATU_ACCOUNT_ADM));

        dm.add(createComponent()
            .setInterface(Object.class.getName(), props)
            .setImplementation(new Object()));
    }

}
