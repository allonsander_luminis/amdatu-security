/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin;

import static java.util.Arrays.asList;
import static org.amdatu.security.account.admin.rest.util.ConfigUtil.*;

import java.util.Dictionary;
import java.util.HashSet;
import java.util.Set;

import org.osgi.service.cm.ConfigurationException;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AccountAdminConfig {

    /**
     * Defines what key in the credentials should be used to identify individual accounts. Defaults to "email".
     */
    public static final String KEY_ACCOUNT_ID_KEY = "keyAccountId";
    /**
     * Defines what credential entries should be considered 'sensitive' and therefore be stored in a hashed manner. Defaults to "password".
     */
    public static final String KEY_SENSITIVE_CREDENTIALS = "sensitiveCredentials";
    /**
     * Defines which credentials should be checked against the credentials stored in the account. Defaults to the union of {@link #KEY_SENSITIVE_CREDENTIALS} and {@link #KEY_ACCOUNT_ID_KEY}.
     */
    public static final String KEY_CHECKED_CREDENTIALS = "checkedCredentials";
    /**
     * The amount of time (in hours) a reset token is valid.
     */
    public static final String KEY_RESET_TOKEN_LIFETIME = "resetTokenLifetime";
    /**
     * Defines if account verification is needed. Defaults to <code>true</code>.
     */
    public static final String KEY_ACCOUNT_VERIFICATION_NEEDED = "accountVerificationNeeded";
    /**
     * Defines if account removal is allowed. Defaults to <code>false</code>.
     */
    public static final String KEY_ACCOUNT_REMOVAL_ALLOWED = "accountRemovalAllowed";

    private static final String DEFAULT_ACCOUNT_ID_KEY = "email";
    private static final String[] DEFAULT_SENSITIVE_CREDENTIALS = { "password" };
    private static final int DEFAULT_RESET_TOKEN_LIFETIME = 12; // hours
    private static final boolean DEFAULT_ACCOUNT_VERIFICATION_NEEDED = true;
    private static final boolean DEFAULT_ACCOUNT_REMOVAL_ALLOWED = false;

    private final String m_accountIdKey;
    private final Set<String> m_sensitiveCredentials;
    private final Set<String> m_checkedCredentials;
    private final int m_resetTokenLifetime;
    private final boolean m_accountVerificationNeeded;
    private final boolean m_accountRemovalAllowed;

    /**
     * Creates a new {@link AccountAdminConfig} instance.
     */
    public AccountAdminConfig() {
        m_accountIdKey = DEFAULT_ACCOUNT_ID_KEY;
        m_sensitiveCredentials = new HashSet<>(asList(DEFAULT_SENSITIVE_CREDENTIALS));
        m_resetTokenLifetime = DEFAULT_RESET_TOKEN_LIFETIME;
        m_accountVerificationNeeded = DEFAULT_ACCOUNT_VERIFICATION_NEEDED;
        m_accountRemovalAllowed = DEFAULT_ACCOUNT_REMOVAL_ALLOWED;
        m_checkedCredentials = new HashSet<>(m_sensitiveCredentials);
        m_checkedCredentials.add(m_accountIdKey);
    }

    /**
     * Creates a new {@link AccountAdminConfig} instance.
     */
    public AccountAdminConfig(Dictionary<String, ?> config) throws ConfigurationException {
        m_accountIdKey = getString(config, KEY_ACCOUNT_ID_KEY, DEFAULT_ACCOUNT_ID_KEY);
        m_sensitiveCredentials = new HashSet<>(asList(getStringArray(config, KEY_SENSITIVE_CREDENTIALS, DEFAULT_SENSITIVE_CREDENTIALS)));
        m_resetTokenLifetime = getInteger(config, KEY_RESET_TOKEN_LIFETIME, DEFAULT_RESET_TOKEN_LIFETIME);
        m_accountVerificationNeeded = getBoolean(config, KEY_ACCOUNT_VERIFICATION_NEEDED, DEFAULT_ACCOUNT_VERIFICATION_NEEDED);
        m_accountRemovalAllowed = getBoolean(config, KEY_ACCOUNT_REMOVAL_ALLOWED, DEFAULT_ACCOUNT_REMOVAL_ALLOWED);

        m_checkedCredentials = new HashSet<>();
        // AMDATUSEC-33 use correct configuration key to obtain checked credentials...
        String[] checkedCreds = getStringArray(config, KEY_CHECKED_CREDENTIALS, null);
        if (checkedCreds != null) {
            m_checkedCredentials.addAll(asList(checkedCreds));
        }
        else {
            m_checkedCredentials.add(m_accountIdKey);
            m_checkedCredentials.addAll(m_sensitiveCredentials);
        }
    }

    /**
     * @return the name of the key used in the credentials to identify the accountId.
     */
    public String getAccountIdKey() {
        return m_accountIdKey;
    }

    /**
     * @return the credentials that should be checked against the account credentials, cannot be <code>null</code>.
     */
    public Set<String> getCheckedCredentials() {
        return m_checkedCredentials;
    }

    /**
     * @return the reset credentials token lifetime, in hours.
     */
    public int getResetTokenLifetime() {
        return m_resetTokenLifetime;
    }

    /**
     * @return the sensitive credentials to hash when creating/updating an account.
     */
    public Set<String> getSensitiveCredentials() {
        return m_sensitiveCredentials;
    }

    /**
     * @return <code>true</code> if accounts are allowed to be removed, <code>false</code> if this operation is prohibited.
     */
    public boolean isAccountRemovalAllowed() {
        return m_accountRemovalAllowed;
    }

    /**
     * @return <code>true</code> if all newly created accounts need to be verified by the owner first, <code>false</code> to force a credential reset immediately after creating an account.
     */
    public boolean isAccountVerificationNeeded() {
        return m_accountVerificationNeeded;
    }
}
