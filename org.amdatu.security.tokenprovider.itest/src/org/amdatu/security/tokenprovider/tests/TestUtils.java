/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.tests;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TestUtils {

    public static Dictionary<String, Object> asDictionary(Object... entries) {
        if (entries == null || (entries.length % 2) != 0) {
            throw new IllegalArgumentException("Need an even number of arguments!");
        }

        Hashtable<String, Object> result = new Hashtable<>();
        for (int i = 0, size = entries.length; i < size; i += 2) {
            String key = entries[i].toString();
            Object val = entries[i + 1];
            if (val != null) {
                result.put(key, val);
            }
        }
        return result;
    }

}
