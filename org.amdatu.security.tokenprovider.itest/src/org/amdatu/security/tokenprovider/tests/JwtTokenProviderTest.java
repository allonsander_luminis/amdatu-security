/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.tests;

import static java.util.Collections.singletonMap;
import static org.amdatu.security.tokenprovider.TokenConstants.SUBJECT;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenProviderException;
import org.amdatu.security.tokenprovider.TokenVerificationInterceptor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JwtTokenProviderTest {

    public static class MyTokenVerificationInterceptor implements TokenVerificationInterceptor {
        @Override
        public void verifyToken(Map<String, String> claims) throws IllegalArgumentException, TokenProviderException {
            if (INVALID_SUBJECT.equals(claims.get(SUBJECT))) {
                throw new InvalidTokenException(Reason.TOKEN_NOT_VALID, "anotherSubject is not allowed any more!",
                    claims);
            }
        }
    }

    public static final String VALID_SUBJECT = "validSubject";
    public static final String INVALID_SUBJECT = "invalidSubject";

    private static final String PID = "org.amdatu.security.tokenprovider.jwt";

    private volatile TokenProvider m_tokenProvider;

    @Before
    public void setUp() throws Exception {
        configure(this)
            .add(createComponent()
                .setInterface(TokenVerificationInterceptor.class.getName(), null)
                .setImplementation(MyTokenVerificationInterceptor.class))
            .add(createConfiguration(PID)
                .setSynchronousDelivery(true)
                .set("algorithm", "HS256")
                .set("issuer", "test token provider")
                .set("audience", "test token audience")
                .set("tokenValidityPeriod", 60 /* seconds */)
                .set("keyUri", "random:HS256"))
            .add(createServiceDependency()
                .setService(TokenProvider.class, "(type=jwt)")
                .setRequired(true))
            .apply();
    }

    @Test
    public void testGenerateToken() throws Exception {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(SUBJECT, VALID_SUBJECT);
        attributes.put("someotherkey", "somevalue");

        String token = m_tokenProvider.generateToken(attributes);

        Map<String, String> verified = m_tokenProvider.verifyToken(token);
        assertEquals(verified.get(SUBJECT), VALID_SUBJECT);
        assertEquals(verified.get("someotherkey"), "somevalue");
    }

    @Test
    public void testGenerateTokenCallsVerificationInterceptor() throws Exception {
        String validToken = m_tokenProvider.generateToken(singletonMap(SUBJECT, VALID_SUBJECT));
        String invalidToken = m_tokenProvider.generateToken(singletonMap(SUBJECT, INVALID_SUBJECT));

        // This should succeed...
        m_tokenProvider.verifyToken(validToken);

        try {
            m_tokenProvider.verifyToken(invalidToken);

            fail("TokenVerificationInterceptor should've been called?!");
        }
        catch (InvalidTokenException e) {
            // Ok, this is expected...
            assertEquals(Reason.TOKEN_NOT_VALID, e.getReason());
        }
    }

    @After
    public void cleanup() {
        cleanUp(this);
    }
}
