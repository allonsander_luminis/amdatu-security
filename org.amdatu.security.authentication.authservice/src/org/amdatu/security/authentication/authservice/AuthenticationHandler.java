/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a service that can be used by developers to verify whether or not a HTTP request is
 * properly authenticated, that is, contains a proper authentication token.
 * <p>
 * This service hides the complexity of dealing with authentication tokens and should be used to
 * determine whether a request is allowed.
 * <p>
 * {@link AuthenticationHandler}s are registered in context of a realm, as such they can be
 * retrieved using filters on the service property <tt>realm</tt> that is set to the name of
 * the configured realm. For example: you can use the filter
 * "<tt>(&(objectClass=org.amdatu.security.authentication.authservice.AuthenticationHandler)(realm=My Realm))</tt>"
 * to obtain the authentication handler for the "<tt>My Realm</tt>" realm.
 */
@ProviderType
public interface AuthenticationHandler {

    /**
     * Can be called to determine whether the given request is allowed or rejected.
     *
     * @param request the HTTP servlet request, cannot be <code>null</code>;
     * @param response the HTTP servlet response, cannot be <code>null</code>.
     * @return an optional {@link RejectInfo} instance. If an empty optional is returned it means
     *         that the authentication handler does not reject the request.
     * @throws IOException in case of I/O problems reading from the request or writing to the given response.
     */
    Optional<RejectInfo> handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
