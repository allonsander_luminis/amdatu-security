/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest.util;

import static org.amdatu.security.tokenprovider.TokenConstants.EXPIRATION_TIME;
import static org.amdatu.security.tokenprovider.TokenConstants.ISSUED_AT;
import static org.amdatu.security.tokenprovider.TokenConstants.NOT_BEFORE;

import java.time.Instant;
import java.util.Map;

import org.amdatu.security.tokenprovider.TokenConstants;

/**
 * Token utility methods.
 */
public class TokenUtil {

    public enum RenewalResult {
            /** Not renewed as the token was still valid. */
            STILL_VALID,
            /** The token has been renewed. */
            RENEWED,
            /** The token has reached its maximum lifetime and cannot be renewed. */
            MAX_LIFETIME_REACHED
    }

    /**
     * Tries the extend the lifetime / validity period of a token with given properties
     * using 'now' as time of reference.
     *
     * @see #extendTokenLifetime(Map, Instant, long)
     */
    public static RenewalResult extendTokenLifetime(Map<String, String> tokenProps, long maxTokenLifetime) {
        return extendTokenLifetime(tokenProps, Instant.now(), maxTokenLifetime);
    }

    /**
     * Tries the extend the lifetime / validity period of a token with given properties
     * using the given instant as time of reference.
     * <p>
     * This method checks if the token is expired, and if so,
     *
     * @param tokenProps the token properties, containing at least the
     *        {@link TokenConstants#ISSUED_AT}, {@link TokenConstants#NOT_BEFORE} and
     *        {@link TokenConstants#EXPIRATION_TIME} attributes.
     * @param referenceTime the reference time to use for the check;
     * @param maxTokenLifetime the maximum lifetime of a token, in seconds.
     * @return the result of the renewal action, never <code>null</code>.
     */
    public static RenewalResult extendTokenLifetime(Map<String, String> tokenProps, Instant referenceTime, long maxTokenLifetime) {
        long refTime = referenceTime.getEpochSecond();

        long issuedAt = parseLong(tokenProps.get(ISSUED_AT));
        long notBefore = parseLong(tokenProps.get(NOT_BEFORE));
        long expirationTime = parseLong(tokenProps.get(EXPIRATION_TIME));

        if ((issuedAt + maxTokenLifetime) <= refTime) {
            return RenewalResult.MAX_LIFETIME_REACHED;
        }
        if ((notBefore >= issuedAt) && (expirationTime > refTime)) {
            return RenewalResult.STILL_VALID;
        }

        // Token is no longer valid, but its lifetime can be extended.
        // Replace the not-before time with the current expiration time,
        // and let the expiration time be updated by our token provider...
        tokenProps.computeIfPresent(NOT_BEFORE, (k, v) -> tokenProps.remove(EXPIRATION_TIME));

        return RenewalResult.RENEWED;
    }

    private static long parseLong(String val) {
        if (val == null || "".equals(val.trim())) {
            return -1L;
        }
        try {
            return Long.parseLong(val);
        }
        catch (NumberFormatException e) {
            return -1L;
        }
    }

}
