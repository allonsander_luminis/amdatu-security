/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.http.context.ServletContextHelper;

/**
 * Defines the Servlet context helper in which the {@link AuthenticationResource} operates.
 */
public class AuthenticationServletContext extends ServletContextHelper {

    private final Bundle m_bundle;
    private final URI m_defaultPage;

    /**
     * Creates a new {@link AuthenticationServletContext} instance.
     */
    public AuthenticationServletContext(AuthenticationRealmConfig config) {
        m_bundle = FrameworkUtil.getBundle(getClass());
        m_defaultPage = config.getLandingPageURL();
    }

    @Override
    public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String pathInfo = request.getPathInfo();
        if ((pathInfo == null || "/".equals(pathInfo)) && m_defaultPage != null) {
            response.sendRedirect(m_defaultPage.toASCIIString());
        }
        return super.handleSecurity(request, response);
    }

    @Override
    public URL getResource(String name) {
        if (name == null) {
            return null;
        }
        return m_bundle.getResource(name);
    }

    @Override
    public Set<String> getResourcePaths(String path) {
        if (path == null) {
            return null;
        }
        Set<String> result = new LinkedHashSet<String>();
        Enumeration<URL> e = m_bundle.findEntries(path, "*", false /* recurse */);
        if (e != null) {
            while (e.hasMoreElements()) {
                result.add(e.nextElement().getPath());
            }
        }
        return result;
    }
}
