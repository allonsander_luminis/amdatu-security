/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local.impl;

import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getInteger;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getMandatoryString;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getMandatoryURI;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getString;
import static org.amdatu.security.authentication.idprovider.local.impl.util.ConfigUtils.getURI;
import static org.amdatu.security.util.crypto.CryptoUtils.createSecureRandom;
import static org.amdatu.security.util.crypto.CryptoUtils.generateSecret;
import static org.amdatu.security.util.crypto.CryptoUtils.getSecretKey;
import static org.jose4j.jwa.AlgorithmConstraints.DISALLOW_NONE;
import static org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256;
import static org.jose4j.jwe.KeyManagementAlgorithmIdentifiers.DIRECT;

import java.io.IOException;
import java.net.URI;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.Dictionary;

import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.amdatu.security.password.util.Hkdf;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.GeneralJwtException;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.AesKey;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.osgi.service.cm.ConfigurationException;

/**
 * Represents the configuration for {@link LocalIdProvider}.
 */
public class LocalIdProviderConfig {
    /**
     * REQUIRED. The identifying name of the local provider, used to lookup and find back local identity providers.
     */
    private static final String KEY_NAME = "name";
    /**
     * REQUIRED. The URI containing or referencing the signing key used for signing the state value of each login.
     */
    public static final String KEY_SIGNING_KEY_URI = "signingKeyUri";
    /**
     * OPTIONAL. The URI containing or referencing the encryption key used for encrypting the state value of each login.
     * If omitted, defaults to the signing key.
     */
    public static final String KEY_ENCRYPTION_KEY_URI = "encryptionKeyUri";
    /**
     * OPTIONAL. The LDAP-style filter to use to select the right {@link LocalIdCredentialsProvider}. Defaults to null which implies that the highest ranked one is used.
     */
    public static final String KEY_ID_CREDENTIAL_PROVIDER_FILTER = "idCredentialProviderFilter";
    /**
     * OPTIONAL. The time window for which a generated authenticator token is valid, in seconds. Defaults to 30 seconds.
     */
    public static final String KEY_TIMESTEP_WINDOW = "timestepWindow";
    /**
     * OPTIONAL. The algorithm used for the {@link SecureRandom} instance. Defaults to using the default algorithm selected by {@code new SecureRandom()}.
     */
    public static final String KEY_PRNG_ALGORITHM = "secureRandomAlgorithm";

    public static final String TYPE_LOCAL = "local";

    public static final String HMAC_SHA256 = "HmacSHA256";

    public static final int DEFAULT_TIMESTEP_WINDOW = 30;

    public static final int CLIENT_KEY_LENGTH = 16;
    public static final int SIGNING_KEY_LENGTH = 32;
    public static final int ENCRYPTION_KEY_LENGTH = 32;

    private static final String TOKEN_ISSUER = "localIdProvider";

    private final String m_name;
    private final String m_idCredentialProviderSelectFilter;
    private final byte[] m_signingKey;
    private final byte[] m_encryptionKey;
    private final SecureRandom m_prng;
    private final int m_timestepWindow;

    public LocalIdProviderConfig(Dictionary<String, ?> config) throws ConfigurationException {
        m_name = getMandatoryString(config, KEY_NAME);
        m_idCredentialProviderSelectFilter = getString(config, KEY_ID_CREDENTIAL_PROVIDER_FILTER, null);
        m_timestepWindow = getInteger(config, KEY_TIMESTEP_WINDOW, DEFAULT_TIMESTEP_WINDOW);

        try {
            m_prng = createSecureRandom(getString(config, KEY_PRNG_ALGORITHM, null));

            URI signKeyURI = getMandatoryURI(config, KEY_SIGNING_KEY_URI);
            URI encKeyURI = getURI(config, KEY_ENCRYPTION_KEY_URI);
            if (encKeyURI == null) {
                encKeyURI = signKeyURI;
            }

            m_signingKey = getSecretKey(m_prng, signKeyURI);
            m_encryptionKey = getSecretKey(m_prng, encKeyURI);
        }
        catch (NoSuchAlgorithmException e) {
            throw new ConfigurationException(null, "unsupported algorithm for PRNG or secret key!", e);
        }
        catch (IOException e) {
            throw new ConfigurationException(KEY_SIGNING_KEY_URI, "invalid file or resource!", e);
        }
    }

    /**
     * @return a random client key, never <code>null</code>.
     */
    public byte[] generateClientKey() {
        return generateSecret(m_prng, CLIENT_KEY_LENGTH);
    }

    /**
     * Generates a state-value that captures the given payload.
     * <p>
     * This method encrypts the given payload using a derived key and returns this value as JWE
     * token using compact serialization.
     * </p>
     *
     * @param timestamp the timestamp to use in the key derivation process;
     * @param clientKey the code generated for each authentication request;
     * @param payload the payload to embed in the generated state value.
     * @return the generated state value.
     */
    public String generateStateCodeAt(Instant timestamp, byte[] clientKey, String payload) {
        Key signKey = new HmacKey(deriveKey(m_signingKey, clientKey, 32));
        Key encKey = new AesKey(deriveKey(m_encryptionKey, clientKey, 32));

        try {
            long now = timestamp.getEpochSecond();

            JwtClaims claims = new JwtClaims();
            claims.setIssuer(TOKEN_ISSUER);
            claims.setAudience(TOKEN_ISSUER);
            claims.setNotBefore(NumericDate.fromSeconds(now));
            claims.setExpirationTime(NumericDate.fromSeconds(now + m_timestepWindow));
            claims.setSubject(payload);

            JsonWebSignature jws = new JsonWebSignature();
            jws.setAlgorithmConstraints(DISALLOW_NONE);
            jws.setAlgorithmHeaderValue("HS256");
            jws.setDoKeyValidation(true);
            jws.setKey(signKey);
            jws.setPayload(claims.toJson());

            JsonWebEncryption jwe = new JsonWebEncryption();
            jwe.setEncryptionMethodHeaderParameter(AES_128_CBC_HMAC_SHA_256);
            jwe.setAlgorithmHeaderValue(DIRECT);
            jwe.setDoKeyValidation(true);
            jwe.setKey(encKey);
            jwe.setPayload(jws.getCompactSerialization());

            return jwe.getCompactSerialization();
        }
        catch (JoseException e) {
            // We only get this exception in case our derived key is not of the expected length!
            throw new IllegalStateException("Invalid configuration for generating state values!", e);
        }
    }

    /**
     * @return the LDAP-style filter to use to select the right {@link LocalIdCredentialsProvider}, can be <code>null</code>.
     */
    public String getIdCredentialProviderFilter() {
        return m_idCredentialProviderSelectFilter;
    }

    /**
     * @return the name of this local identity provider, never <code>null</code>.
     */
    public String getName() {
        return m_name;
    }

    /**
     * @param clientKey the code generated for each authentication request;
     * @param state the actual state value to verify.
     * @return the payload of the given state value.
     */
    public String verifyStateCodeAt(Instant timestamp, byte[] clientKey, String state) {
        Key signKey = new HmacKey(deriveKey(m_signingKey, clientKey, 32));
        Key encKey = new AesKey(deriveKey(m_encryptionKey, clientKey, 32));

        try {
            JsonWebEncryption jwe = new JsonWebEncryption();
            jwe.setKey(encKey);
            jwe.setCompactSerialization(state);

            String jwt = jwe.getPayload();

            JwtConsumer consumer = new JwtConsumerBuilder()
                .setJwsAlgorithmConstraints(DISALLOW_NONE)
                .setExpectedAudience(TOKEN_ISSUER)
                .setExpectedIssuer(TOKEN_ISSUER)
                .setRequireExpirationTime()
                .setRequireNotBefore()
                .setRequireSubject()
                .setVerificationKey(signKey)
                .setEvaluationTime(NumericDate.fromSeconds(timestamp.getEpochSecond()))
                .build();

            JwtClaims jwtClaims = consumer.processToClaims(jwt);

            return jwtClaims.getSubject();
        }
        catch (JoseException | InvalidJwtException | GeneralJwtException e) {
            // The contents were bogus or too old, do not return anything...
            return null;
        }
    }

    private byte[] deriveKey(byte[] ikm, byte[] salt, int length) {
        Hkdf kdf = new Hkdf.Builder()
            .setAlgorithm(HMAC_SHA256)
            .setInitialKeyingMaterial(ikm)
            .setSalt(salt)
            .build();

        return kdf.deriveKey(null, length);
    }
}
