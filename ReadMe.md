# Amdatu Security

The security component offers authorization features. It makes it easy to implement
authorization for web applications, indepent of how users and groups are created and
stored. The service provided by this component is the TokenProvider and is used to create
secure tokens from a set of attributes. This is usally done after a user has been
successfully logged in.

This component provides a default token provider implementation that is based on the [Web
Token specification](https://tools.ietf.org/html/rfc7519) specification.

## Usage

Please visit the [Security](http://www.amdatu.org/components/security.html) component page
on the Amdatu website for a detailed description on how to use Amdatu Security.

## Building

The source code can be built on the command line using Gradle, or by using
[Bndtools](http://bndtools.org/) in Eclipse.  Tests can be ran from command line using
Gradle as well, or by using JUnit in Eclipse.

### Eclipse using Bndtools

When Bndtools is correctly installed in Eclipse, import all subprojects into the workspace
using `Import -> Existing Projects into Workspace` from the Eclipse context menu. Ensure
`project -> Build Automatically` is checked in the top menu, and when no (compilation)
errors are present, each subproject's bundles should be available in their `generated`
folder.

###Gradle

When building from command line, invoke `./gradlew jar` from the root of the project. This
will assemble all project bundles and place them in the `generated` folder of each
subproject.


## Testing

### Eclipse using Bndtools

Unit tests can be ran per project or per class, using JUnit in Eclipse. Unit tests are ran
by right clicking a project which contains a non-empty test folder or a test class and
choosing `Run As -> JUnit Test`.

Integration tests can be ran in a similar way; right click an integration test project
(commonly suffixed by .itest), but select `Run As -> Bnd OSGi Test Launcher (JUnit)`
instead.

### Gradle

Unit tests are ran by invoking `./gradlew test` from the root of the project.

Integration tests can be ran by running a full build, this is done by invoking `./gradlew
build` from the root of the project.  More info on available Gradle tasks can be found by
invoking `./gradlew tasks`.


## Links

* [Amdatu Website](http://www.amdatu.org/components/security.html);
* [Source Code](https://bitbucket.org/amdatu/amdatu-security);
* [Issue Tracking](https://amdatu.atlassian.net/browse/AMDATUSEC);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Security);
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/AMDATUSEC).

## License

The Amdatu Security project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html). 