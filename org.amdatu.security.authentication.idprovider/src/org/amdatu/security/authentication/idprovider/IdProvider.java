/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.tokenprovider.InvalidTokenException;

/**
 * Denotes a remote identity provider, that allows a user to be authenticated and thereby confirm
 * his/her identity.
 */
public interface IdProvider {

    /**
     * Creates an URI that can be used by a user-agent to access the authorization/authentication
     * page of the (remote) {@link IdProvider}.
     *
     * @param callbackURI the URI to pass to the remote identity provider to callback on in case
     *        of success or failure, cannot be <code>null</code>;
     * @param params the request parameters that can be used to determine what the location of the
     *        authorization URI should be, never <code>null</code>.
     * @return the URL to the authorization endpoint, can be an external URL, cannot be <code>null</code>.
     */
    URI getAuthenticationURI(URI callbackURI, Map<String, String[]> params);

    /**
     * Creates a URI that can be used by a user-agent to end the session on the (remote) {@link IdProvider}.
     *
     * @param callbackURI the callback URI to pass to the remote {@link IdProvider};
     * @param credentials the credentials that identify the current user, cannot be <code>null</code>.
     * @return a end-session URI, if the (remote) {@link IdProvider} supports this, or an empty result otherwise.
     */
    default Optional<URI> getEndSessionURI(URI callbackURI, Map<String, String> credentials) {
        return Optional.empty();
    }

    /**
     * Creates a URI that can be used by a user-agent to end the session on the
     * (remote) {@link IdProvider}.
     *
     * @param callbackURI the callback URI to pass to the remote {@link IdProvider};
     * @param credentials the credentials that identify the current user, cannot be
     *                    <code>null</code>.
     * @param state       If a valid post_logout_redirect_uri is passed, then the
     *                    client may also send a state parameter. This will be
     *                    returned back to the client as a query string parameter
     *                    after the user redirects back to the client. This is
     *                    typically used by clients to round-trip state across the
     *                    redirect.
     * @return a end-session URI, if the (remote) {@link IdProvider} supports this,
     *         or an empty result otherwise.
     */
    default Optional<URI> getEndSessionURI(URI callbackURI, Map<String, String> credentials, String state) {
        return Optional.empty();
    }

    /**
     * @return the type of identity provider, never <code>null</code>.
     */
    String getType();

    /**
     * Determines the user identity after successful authentication with the (remote) identity
     * provider.
     *
     * @param callbackURI the URI to pass to the remote identity provider to callback on in case of
     *        success or failure, cannot be <code>null</code>;
     * @param params the request parameters that can be used to the user's identity, never
     *        <code>null</code>.
     * @return the attributes describing the identity of the user, cannot be <code>null</code>.
     */
    Map<String, String> getUserIdentity(URI callbackURI, Map<String, String[]> params);

    /**
     * @return <code>true</code> if the given token credentials were refreshed (modified),
     *         <code>false</code> otherwise.
     * @throws InvalidTokenException in case the given credentials didn't yield a token that was
     *         accepted by the ID provider.
     */
    default boolean refreshToken(Map<String, String> credentials) throws InvalidTokenException {
        return false;
    }

}
