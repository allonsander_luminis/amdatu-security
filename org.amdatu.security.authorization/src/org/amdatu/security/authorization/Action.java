/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents a type of Action for which access is requested.
 *
 * Typical examples include CRUD and BREAD actions, as well as execution of a particular service function
 */
@ConsumerType
public interface Action {

    /**
     * @return the name of this action
     */
    // using name() instead of getName() to facilitate the use of enums as Action implementations
    public String name();
}
